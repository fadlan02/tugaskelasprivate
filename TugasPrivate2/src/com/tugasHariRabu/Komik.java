package com.tugasHariRabu;

public class Komik extends Buku{
protected int hargaKomikPerhari = 2000;

    public int getHargaKomikPerhari() {
        return hargaKomikPerhari;
    }

    public void setHargaKomikPerhari(int hargaKomikPerhari) {
        this.hargaKomikPerhari = hargaKomikPerhari;
    }
}
