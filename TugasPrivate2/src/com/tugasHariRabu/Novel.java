package com.tugasHariRabu;

public class Novel extends Buku{
protected int hargaNovelPerhari = 5500;

    public int getHargaNovelPerhari() {
        return hargaNovelPerhari;
    }

    public void setHargaNovelPerhari(int hargaNovelPerhari) {
        this.hargaNovelPerhari = hargaNovelPerhari;
    }
}
