package com.tugasHariRabu;

public class TagihanListrik {
    int noPelanggan;
    String namaPelanggan;
    double pulsaPemakaian;
    String bulanTagihan;
    double tarif;
    double biaya;
    double tagihan;

    TagihanListrik(int noPelanggan, String namaPelanggan, double pulsaPemakaian, String bulanTagihan, double tarif, double biaya){
        this.noPelanggan = noPelanggan;
        this.namaPelanggan = namaPelanggan;
        this.pulsaPemakaian = pulsaPemakaian;
        this.bulanTagihan = bulanTagihan;
        this.tarif = tarif;
        this.biaya = biaya;

        System.out.println("No pelanggan : " + noPelanggan);
        System.out.println("Nama pelanggan : " + namaPelanggan);
        System.out.println("Pulsa : " + pulsaPemakaian);
        System.out.println("Bulan Tagihan : " + bulanTagihan);
        System.out.println("Tarif : " + tarif);
        System.out.println("Biaya : " + biaya);
    }


public void bayarTagihan(){
    this.tagihan = (pulsaPemakaian * tarif) + biaya;
    System.out.println("Tagihan pembayaran : " + tagihan);
}

    public static void main(String[] args) {
     TagihanListrik aldo = new TagihanListrik(12,"aldo",150,"juli",250,20000);
     aldo.bayarTagihan();
    }
}
