package com.tugasHariRabu;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;

public class PengurutanData {
    public static void main(String[] args) {
        Integer [] arr = {5,1,2,3,4};
        System.out.println("Data Asli");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "\t");
        }
        System.out.println();

        System.out.println("Pengurutan Ascending");
        Arrays.sort(arr);
        for (int i = 0; i < 1 ; i++) {
            System.out.println(Arrays.toString(arr));
        }
        System.out.println();

        System.out.println("Pengurutan Descending");
        Arrays.sort(arr, Collections.reverseOrder());
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "\t");
        }


    }
}
