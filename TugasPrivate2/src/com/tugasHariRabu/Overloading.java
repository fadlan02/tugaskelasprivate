package com.tugasHariRabu;

public class Overloading {

    public void add(int a){
        System.out.println("Minimum Value = " + a);
    }
    public void add(float b){
        System.out.println("Minimum Value = " + b);
    }

    public static void main(String[] args) {
    Overloading tes = new Overloading();
    tes.add(6);
    tes.add(7.3F);
    }
}
