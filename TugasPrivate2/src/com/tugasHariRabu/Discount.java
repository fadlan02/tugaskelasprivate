package com.tugasHariRabu;

import java.util.Scanner;

public class Discount {
    int harga;
    int discount;
    int hargaBayar;

    public void hasilDiscount(){
        this.discount = harga / 100 * 10;
        System.out.println("Discount : Rp. " + discount);
    }

    public void hasilHargaBayar(){
        this.hargaBayar = harga - discount;
        System.out.println("Harga bayar : Rp. " + hargaBayar);
    }


    public static void main(String[] args) {
        Discount tes = new Discount();

        Scanner scan = new Scanner(System.in);
        System.out.print("Masukan Harga : Rp. ");
        tes.harga = scan.nextInt();
        tes.hasilDiscount();
        tes.hasilHargaBayar();
    }
}
