package com.tugasHariRabu;

import java.util.Scanner;

public class MenuRestaurant {
    public static void main(String[] args) {
        System.out.println("\t\t Menu Restaurant Mc'Cihuy ");
        System.out.println("====================================");
        System.out.println("1. Nasi Goreng Informatika\t\t Rp. 5.000,-");
        System.out.println("2. Nasi Soto Ayam Internet\t\t Rp. 7.000,-");
        System.out.println("3. Gado-Gado Disket\t\t\t\t Rp. 4.500,-");
        System.out.println("4. Bubur Ayam LAN\t\t\t\t Rp. 4.000,-");
        System.out.println("====================================");


        Scanner scan = new Scanner(System.in);
        System.out.print("Masukan Pilihan Anda : ");
        String input = scan.nextLine();

        switch (input){
            case "1" :
                System.out.println("Pilihan No." + input + " Nasi Goreng Informatika Rp. 5.000,-" );
                break;
            case "2" :
                System.out.println("Pilihan No." + input + " Nasi Soto Ayam Internet Rp. 7.000,-" );
                break;
            case "3" :
                System.out.println("Pilihan No." + input + " Gado-Gado Disket Rp. 4.500,-");
                break;
            case "4":
                System.out.println("Pilihan No." + input + " Bubur Ayam LAN Rp. 4.000,-");
                break;
            default:
                System.out.println("Pilihan yang anda masukan tidak sesuai");
        }
    }
}
