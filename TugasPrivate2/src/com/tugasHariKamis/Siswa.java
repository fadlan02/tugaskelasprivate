package com.tugasHariKamis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Siswa {
     private int id;
     private String nama;
     private int nilai;

    Siswa(int id, String nama, int nilai){
        this.id = id;
        this.nama = nama;
        this.nilai = nilai;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public int getNilai() {
        return nilai;
    }
}

class Menu{
    Scanner scan = new Scanner(System.in);
    int pilihan;
    ArrayList<Siswa> smk = new ArrayList<Siswa>();

    void createObj() {
        int jumlahData = 0;
        System.out.println("Masukan Jumlah Data yang Di inginkan : ");
//        jumlahData = scan.nextInt();

            System.out.print("Masukan Id anda : ");
            int id = scan.nextInt();

            System.out.print("Masukan nama anda : ");
            String nama = scan.next();

            System.out.print("Masukan Nilai : ");
            int nilai = scan.nextInt();

            Siswa sekolah1 = new Siswa(id, nama, nilai);
            smk.add(sekolah1);

        System.out.println("Kembali ke menu : n/y");
            String pilih = scan.next();
                    if(pilih.equals("y")){
                        show();
                    }
        for (Siswa arraylist:smk) {
            System.out.print(arraylist.getId() +"\t\t"+ arraylist.getNama() +"\t\t"+ arraylist.getNilai());
        }
    }

    void editDataSiswa(){

    }

    void removeObjectSiswa(){

    }

    void laporanDataSiswa(){
        System.out.println("===============================");
        System.out.println("Id\t\tNama\t\tNilai");
        System.out.println("===============================");

    }


    void show() {
        System.out.println("============ > Menu < ============");
        System.out.println("1. Create Object ");
        System.out.println("2. Edit Data Siswa ");
        System.out.println("3. Remove Object");
        System.out.println("4. Laporan Data Siswa");
        System.out.println("5. Exit");

        System.out.println("Masukan Pilihan Anda : ");
        this.pilihan = scan.nextInt();
        switch (pilihan) {
            case 1:
                createObj();
                break;
            case 2:
                editDataSiswa();
                break;
            case 3:
                removeObjectSiswa();
                break;
            case 4:
                laporanDataSiswa();
                break;
            case 5:
                show();
            default:
                System.out.println("salah");
        }
    }

    public static void main(String[] args) throws IOException {
        Menu tes = new Menu();
        tes.show();
    }
}
