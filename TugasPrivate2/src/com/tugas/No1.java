package com.tugas;
// membuat class
class Hewan{
    String warna = "merah";
}
public class No1 {
// Object adalah sesuatu yang memiliki status dan perilaku (behaviour)
// Class adalah tempat untuk menampung objek dan method

    public static void main(String[] args) {
        // membuat object
        Hewan kucing = new Hewan();
        System.out.println("kucing saya berwarna : " + kucing.warna);
    }
}
