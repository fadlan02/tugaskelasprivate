package com.tugas;

import java.util.Scanner;

public class No4 {
    double panjang;
    double lebar;
    double tinggi;
    double r ;
    double phi = 3.14;
    double volumeBalok;
    double volumeKubus;
    double volumeBola;
    double rusuk;
    double average;
    double sum;

    void balok(){
        volumeBalok = panjang * lebar * tinggi;
        System.out.println("Volume balok adalah : " + volumeBalok);
    }

    void bola(){
         volumeBola = phi * r * r * r * 3 / 4;
        System.out.println("Volume bola adalah : " + volumeBola);
    }

    void kubus(){
        volumeKubus = rusuk * rusuk * rusuk;
        System.out.println("Volume kubus adalah : " + volumeKubus);
    }

    void rataRata(){
        average = volumeBalok + volumeKubus + volumeBola / 3 ;
        System.out.println("Rata rata nya adalah : " + average);
    }

    void jumlah(){
        sum = volumeBalok + volumeKubus + volumeBola;
        System.out.println("Summary : " + sum);
    }

    public static void main(String[] args) {

        No4 tes = new No4();
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukan panjang : ");
        tes.panjang = scan.nextDouble();

        System.out.print("Masukan lebar : ");
        tes.lebar = scan.nextDouble();

        System.out.print("Masukan Tinggi : ");
        tes.tinggi = scan.nextDouble();

        System.out.println("Masukan rusuk : ");
        tes.rusuk = scan.nextDouble();

        System.out.println("masukan radius : ");
        tes.r = scan.nextDouble();


       tes.balok();
       tes.kubus();
       tes.bola();
       tes.jumlah();
       tes.rataRata();
    }
}
